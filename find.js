function find(elements, cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.

    let elementCount = 0;

    for(let index = 0; index < elements.length; index++) {
        let isPresent = cb(elements[index], index);

        if (isPresent === true) {
            return elements[index];
            break;
        } else {
            elementCount += 1;
        }
    }

    if(elementCount === elements.length) {
        return undefined;
    }
}

module.exports = find;

