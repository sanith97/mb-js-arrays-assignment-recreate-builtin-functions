function reduce(elements, cb, startingValue) {

    if(elements.length === 0 && startingValue === undefined) {
        return [];
    }

    let preValue = (startingValue === undefined ? 0 : startingValue);
    
    for(let index = 0; index < elements.length; index++) {
        preValue = cb(preValue, elements[index], index, elements);

    };    

    return preValue;

}

module.exports = reduce;
