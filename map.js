// _.map([1, 2, 3], function(num){ return num * 3; });
// => [3, 6, 9]
// _.map({one: 1, two: 2, three: 3}, function(num, key){ return num * 3; });
// => [3, 6, 9]
// _.map([[1, 2], [3, 4]], _.first);
// => [1, 3]


// const newList = List1.map(function(num, index) {
//     return (index);
// });

// console.log(newList);


function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
    let newArray = []
    for (let index = 0; index < elements.length; index++) {
        
        let newMappedItem = cb(elements[index], index, elements);
        newArray.push(newMappedItem);
    }
    return newArray;
}

module.exports = map;

