function flatten(elements, depth = 1) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

    let requiredArray = [];

    for(let index = 0; index < elements.length; index++) {
        if(Array.isArray(elements[index]) && depth > 0) {
            requiredArray = requiredArray.concat(flatten(elements[index], depth - 1));

        } else if (elements[index] === undefined) {
            continue;
        }
        else {
            requiredArray.push(elements[index]);
        }
    };

    return requiredArray;

    
};

module.exports = flatten;