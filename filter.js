function filter(elements, cb) {
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test

    let requiredArray = [];

    for(let index = 0; index < elements.length; index++) {
        let returnedValue = cb(elements[index], index,elements);

        if(returnedValue === true) {
            requiredArray.push(elements[index]);
        }
    };

    if(requiredArray.length === 0) {
        return [];
    } else {
        return requiredArray;
    }

}


module.exports = filter;
